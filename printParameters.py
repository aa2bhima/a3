import socket

def find_free_port():
    s = socket.socket()
    s.bind(('', 0))            # Bind to a free port provided by the host.
    return s.getsockname()[1]  # Return the port number assigned.

# Use this to find out the host/ip address.
print(socket.gethostbyname(socket.gethostname()))
print("nse: " + str(find_free_port()))
print("router1: " + str(find_free_port()))
print("router2: " + str(find_free_port()))
print("router3: " + str(find_free_port()))
print("router4: " + str(find_free_port()))
print("router5: " + str(find_free_port()))